package com.banregio.springboot.web.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.banregio.springboot.web.app.entity.Producto;
import com.banregio.springboot.web.app.repository.ProductoRepository;

@Service
public class ProductoService {
	@Autowired
	private ProductoRepository productoRepository;
	
	public List<Producto> getAll(){
		return productoRepository.findAll();
	}
	
	public Optional<Producto> getById(Long id){
		return productoRepository.findById(id);
	}
	
	public void insertProducto(Producto producto){
		try {
		productoRepository.save(producto);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void updateProducto(Long id,Producto producto){
		try {
		producto.setId(id);
		productoRepository.save(producto);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public void deleteProducto(Long id){
		try {
		
		productoRepository.deleteById(id);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
