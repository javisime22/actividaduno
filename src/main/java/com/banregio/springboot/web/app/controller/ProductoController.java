package com.banregio.springboot.web.app.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.banregio.springboot.web.app.entity.Producto;
import com.banregio.springboot.web.app.service.ProductoService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")

public class ProductoController {
	@Autowired
	private ProductoService productoService;
	@ApiOperation(value = "Api para obtener productos", notes="Api para obtener la lista de productos")
	@GetMapping("/producto")
	public ResponseEntity<?> getProductos(){
		try {
			List<Producto> getProductos = this.productoService.getAll();
			return new ResponseEntity<>(getProductos, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@ApiOperation(value = "Api para obtener producto por id", notes="Api para obtener un producto mediante id")
	@GetMapping("/producto/{id}")
	public ResponseEntity<?> getProductoById(@PathVariable Long id){
		try {
			Optional<Producto> getProducto = this.productoService.getById(id);
			return new ResponseEntity<>(getProducto, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@ApiOperation(value = "Api para insertar productos", notes="Api para hacer el insert mediante el body")
	@PostMapping("/producto")
	public ResponseEntity<?> insertProducto(@RequestBody Producto producto){
		try {
			this.productoService.insertProducto(producto);
			
			return new ResponseEntity<>(producto, HttpStatus.CREATED);
		}catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@ApiOperation(value = "Api para actualizar producto", notes="Api para hacer el update mediante el id")
	@PutMapping("/producto/{id}")
	public ResponseEntity<?> updateProducto(@PathVariable long id,@RequestBody Producto producto){
		try {
			this.productoService.updateProducto(id,producto);
			
			return new ResponseEntity<>(producto, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@ApiOperation(value = "Api para eliminar productos", notes="Api para eliminar mediante el id")
	@DeleteMapping("/producto/{id}")
	public ResponseEntity<?> deleteProducto(@PathVariable long id){
		try {
			this.productoService.deleteProducto(id);
			
			return new ResponseEntity<>(id, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
