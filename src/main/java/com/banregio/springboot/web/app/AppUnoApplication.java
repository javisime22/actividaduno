package com.banregio.springboot.web.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients

public class AppUnoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppUnoApplication.class, args);
	}

}
