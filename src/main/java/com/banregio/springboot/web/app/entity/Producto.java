package com.banregio.springboot.web.app.entity;



import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "producto")
@Data
public class Producto {

    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column (name = "nombre")
    private String nombre;
    
    @Column (name = "precio")
    private double precio;
    
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_categoria", referencedColumnName = "id")
    private Categoria categoria;
    
}
