package com.banregio.springboot.web.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.banregio.springboot.web.app.entity.Categoria;
import com.banregio.springboot.web.app.repository.CategoriaRepository;

@Service
public class CategoriaService {
	@Autowired
	private CategoriaRepository categoriaRepository;

	public List<Categoria> getAll() {
		return categoriaRepository.findAll();
	}

	public Optional<Categoria> getById(Long id) {
		return categoriaRepository.findById(id);
	}

	public void insertCategoria(Categoria categoria) {
		categoriaRepository.save(categoria);
	}

	public void updateCategoria(Long id, Categoria categoria) {
		try {
			categoria.setId(id);
			categoriaRepository.save(categoria);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void deleteCategoria(Long id) {
		try {
			categoriaRepository.deleteById(id);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
