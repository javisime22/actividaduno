package com.banregio.springboot.web.app.controller;

import com.banregio.springboot.web.app.entity.Producto;
import com.banregio.springboot.web.app.repository.VentaRepository;

import io.swagger.annotations.ApiOperation;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 980022884
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class VentaController {
    @Autowired 
    private VentaRepository ventaRepository;
    @ApiOperation(value = "Api de venta", notes="Api para consumir los productos mediante feign")
    @GetMapping("/venta")
	public ResponseEntity<?> getProductos(){
		try {
			List<Producto> getProductos = this.ventaRepository.getAll();
			return new ResponseEntity<>(getProductos, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
