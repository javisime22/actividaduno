package com.banregio.springboot.web.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.banregio.springboot.web.app.entity.Categoria;

@Repository
public interface CategoriaRepository extends JpaRepository<Categoria,Long>{

}
