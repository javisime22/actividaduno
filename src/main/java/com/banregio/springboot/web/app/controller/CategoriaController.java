package com.banregio.springboot.web.app.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.banregio.springboot.web.app.entity.Categoria;
import com.banregio.springboot.web.app.service.CategoriaService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class CategoriaController {
	@Autowired
	private CategoriaService categoriaService;
	@ApiOperation(value = "Api obtener categorias", notes="Api para consumir las categrias")
	@GetMapping("/categoria")
	public ResponseEntity<?> getCategorias(){
		try {
			List<Categoria> getCategoria = this.categoriaService.getAll();
			return new ResponseEntity<>(getCategoria, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@ApiOperation(value = "Api obtener una categoria", notes="Api para obtener la categoria por id")
	@GetMapping("/categoria/{id}")
	public ResponseEntity<?> getCategoriaById(@PathVariable Long id){
		try {
			Optional<Categoria> getCategoria = this.categoriaService.getById(id);
			return new ResponseEntity<>(getCategoria, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@ApiOperation(value = "Api para insertar una categoria", notes="Api para insertar las categorias mediante el body")
	@PostMapping("/categoria")
	public ResponseEntity<?> insertCategoria(@RequestBody Categoria categoria){
		try {
			this.categoriaService.insertCategoria(categoria);
			
			return new ResponseEntity<>(categoria, HttpStatus.CREATED);
		}catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@ApiOperation(value = "Api para actualizar una categoria", notes="Api para hacer el update")
	@PutMapping("/categoria/{id}")
	public ResponseEntity<?> updateCategoria(@PathVariable long id,@RequestBody Categoria categoria){
		try {
			this.categoriaService.updateCategoria(id,categoria);
			
			return new ResponseEntity<>(categoria, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@ApiOperation(value = "Api para eliminar", notes="Api para elminar una categoria")
	@DeleteMapping("/categoria/{id}")
	public ResponseEntity<?> deleteCategoria(@PathVariable long id){
		try {
			this.categoriaService.deleteCategoria(id);
			
			return new ResponseEntity<>(id, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
