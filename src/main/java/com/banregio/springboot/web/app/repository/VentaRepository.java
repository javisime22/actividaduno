package com.banregio.springboot.web.app.repository;

import com.banregio.springboot.web.app.entity.Producto;
import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;


@FeignClient(name = "venta",url = "${url-feign}")
public interface VentaRepository {
    @GetMapping("/producto")
    public List<Producto> getAll();
}
